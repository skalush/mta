## Before Opening a Merge request

- **Could this be an issue?:** Extremely small merge requets better serve as issues, especially if they are only a couple of lines. I also look at issues more often than merge requests.
- **Only offer one feature:** Make sure you do not commit several, disconnected features within the same merge request. Try to be as concise as possible.
- **Explain what you did:** Detail exactly what you updated. Some screenshots also help.

If your merge request was not accepted, that does not mean it has not been noticed.